package com.example.webClientGroupServerJwtDiako;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepo extends JpaRepository<GroupEntity, String> {

        GroupEntity findGroupEntityById(String id);
        GroupEntity findGroupEntityByName(String name);

}

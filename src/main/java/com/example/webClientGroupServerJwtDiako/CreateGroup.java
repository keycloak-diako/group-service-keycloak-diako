package com.example.webClientGroupServerJwtDiako;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateGroup {
    String name;
    @JsonCreator
    public CreateGroup(@JsonProperty String name) {
        this.name = name;
    }
}

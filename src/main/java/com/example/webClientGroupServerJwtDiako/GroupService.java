package com.example.webClientGroupServerJwtDiako;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupService {
    @Autowired
    GroupRepo groupRepo;

    public GroupService(GroupRepo groupRepo) {
        this.groupRepo = groupRepo;
    }

    public GroupEntity createGroup(CreateGroup createGroup) {
        GroupEntity group = new GroupEntity(createGroup.getName());
        groupRepo.save(group);
        return group;
    }

    public String getGroupName(String id) {
        return groupRepo.findGroupEntityById(id).getName();
    }

    public List<GroupEntity> getAllGroups(){
        return groupRepo.findAll();
    }

    public GroupEntity updateGroupName(String id, String name) {
        GroupEntity groupEntity = groupRepo.findGroupEntityById(id);
        groupEntity.setName(name);
        groupRepo.save(groupEntity);
        return groupEntity;
    }

    public GroupEntity deleteGroup(String id) {
        GroupEntity groupEntity = groupRepo.findGroupEntityById(id);
        groupRepo.deleteById(id);
        return groupEntity;
    }

    public GroupEntity findGroupByName(String name) {
        return groupRepo.findGroupEntityByName(name);
    }

    public GroupEntity addUserIdToGroupUserLis(String groupName, String id) {
        GroupEntity group= groupRepo.findGroupEntityByName(groupName);
        group.getUserIds().add(id);
        return groupRepo.save(group);
    }
}

package com.example.webClientGroupServerJwtDiako;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
public class GroupEntity {
    @Id
    String id;
    String name;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    List<String> userIds;

    @JsonCreator
    public GroupEntity(@JsonProperty("name") String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.userIds = new ArrayList<>();
    }
}

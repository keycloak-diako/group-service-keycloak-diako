package com.example.webClientGroupServerJwtDiako;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WebClientGroupServerJwtDiakoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebClientGroupServerJwtDiakoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demoData(GroupRepo groupRepo) {
		return args -> {
			groupRepo.save(new GroupEntity("Maddes Grupp"));
			groupRepo.save(new GroupEntity("Eddas Grupp"));
			groupRepo.save(new GroupEntity("Diakos Grupp"));
		};
	}
}

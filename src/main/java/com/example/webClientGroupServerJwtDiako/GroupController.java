package com.example.webClientGroupServerJwtDiako;

import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/groups")
public class GroupController {


    GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }


    @PostMapping("/create_group")
    public GroupEntity createGroup(@RequestBody CreateGroup createGroup) {
        return groupService.createGroup(createGroup);
    }


    @GetMapping("/find_group_name/{id}")
    public String findGroupName(@PathVariable("id") String id) {
        return groupService.getGroupName(id);
    }


    @GetMapping("/find_group_by_name/{name}")
    public GroupEntity findGroupByName(@PathVariable("name") String name) {
        return groupService.findGroupByName(name);
    }


    @GetMapping("/all_groups")
    public List<GroupEntity> getAllGroups() {
        return groupService.getAllGroups();
    }


    @PostMapping("/update_name/{id}")
    public GroupEntity updateGroupName(@PathVariable("id") String id, @RequestParam(value = "name", required = true) String name) {
        return groupService.updateGroupName(id, name);
    }


    @DeleteMapping("/{id}/delete")
    public GroupEntity deleteGroup (@PathVariable("id") String id) {
        return groupService.deleteGroup(id);
    }


    @PostMapping("/add_user_to_group/{group_name}/{userId}")
    public GroupEntity addUserIdToGroup(
            @PathVariable ("group_name") String groupName,
            @PathVariable ("userId") String userId
    ){
        return groupService.addUserIdToGroupUserLis(groupName,userId);
    }
}
